(ns plf01.core)

(defn función-<-1 
  [a b]
  (< a b))

(defn función-<-2
  [a b c]
  (< a b c))

(defn función-<-3
  [a b c d]
  (< a b c d))

(función-<-1 1 4)
(función-<-2 1 2 3)
(función-<-3 1 2 3 4)

(defn funcion-<=-1
  [a b]
  (<= a b))

(defn funcion-<=-2
  [a b c]
  (<= a b c))

(defn funcion-<=-3
  [a b c d]
  (<= a b c d))

(funcion-<=-1 1 4)
(funcion-<=-2 1 2 3)
(funcion-<=-3 1 2 3 4)

(defn funcion-==-1
  [a b]
  (== a b))

(defn funcion-==-2
  [a b c]
  (== a b c))

(defn funcion-==l-3
  [a b c d]
  (= a b c d))

(funcion-==-1 1 4)
(funcion-==-2 1 2 3)
(funcion-==l-3 1 2 3 4)

(defn funcion->-1
  [a b]
  (> a b))

(defn funcion->-2
  [a b c]
  (> a b c))

(defn funcion->-3
  [a b c d]
  (> a b c d))

(funcion->-1 1 4)
(funcion->-2 1 2 3)
(funcion->-3 1 2 3 4)

(defn funcion->=-1
  [a b]
  (>= a b))

(defn funcion->=-2
  [a b c]
  (>= a b c))

(defn funcion->=-3
  [a b c d]
  (>= a b c d))

(funcion->=-1 1 4)
(funcion->=-2 1 2 3)
(funcion->=-3 1 2 3 4)

(defn funcion-assoc-1
  [a b c]
  (assoc [a] b c))

(defn funcion-assoc-2
  [a b  c d]
  (assoc [a b] c d))

(defn funcion-assoc-3
  [a b c A B]
  (assoc [a b c] A B))

(funcion-assoc-1 [1] 0 10)
(funcion-assoc-2 1 2 0 10)
(funcion-assoc-3 1 2 3 0 10)

(defn funcion-assoc-in-1
  [a A B]
  (assoc-in {} [a] {A B}))

(defn funcion-assoc-in-2
  [a b A B]
  (assoc-in {} [a b] {A B}))

(defn funcion-assoc-in-3
  [a b c  A B]
  (assoc-in {} [a b c] {A B}))

(funcion-assoc-in-1 {} [1] {1 2})
(funcion-assoc-in-2  1 2 1 2)
(funcion-assoc-in-3  1 2  3 1 2)

(defn funcion-concat-1
  [a b A B]
  (concat [a b] [A B]))

(defn funcion-concat-2
  [a b c A B]
  (concat [a b c] [A B]))

(defn funcion-concat-3
  [a b c d A B C D]
  (concat [a b c d] [A B C D]))

(funcion-concat-1 1 2 3 2)
(funcion-concat-2 1 2 3 2 4)
(funcion-concat-3 1 2 3 4 6 7 8 9)

(defn funcion-conj-1
  [a A]
  (conj [a] A))

(defn funcion-conj-2
  [a b A B]
  (conj [a b] A B))

(defn funcion-conj-3
  [a b c A B C]
  (conj [a b c] [A B C]))

(funcion-conj-1 [\a] \A)
(funcion-conj-2 \a \B \b \A)
(funcion-conj-3 \a \b \c \A \B \C)

(defn funcion-cons-1
  [a A]
  (cons [a] [A]))

(defn funcion-cons-2
  [a b A B]
  (cons [a b] [A B]))

(defn funcion-cons-3
  [a b c d A B C D]
  (cons [a b c d] [A B C D]))

(funcion-cons-1 \a \A)
(funcion-cons-2 \a \B \b \A)
(funcion-cons-3 \a \b \c \d \A \B \C \D)

(defn funcion-contains?-1
  [a b A]
  (contains? {a b} A))

(defn funcion-contains?-2
  [a b c  d A]
  (contains? {a b c d} A))

(defn funcion-contains?-3
  [a b c  d  A B C]
  (contains? {a b c d A B} C))

(funcion-contains?-1 \a \b \a)
(funcion-contains?-2 \a \b \c \d  \c)
(funcion-contains?-3 \a \b \c \d \A \B \C)

(defn funcion-count-1
  [a]
  (count a))

(defn funcion-count-2
  [a]
  (count a))

(defn funcion-count-3
  [a]
  (count a))

(funcion-count-1 [\a])
(funcion-count-1 (vector \a \b \c))
(funcion-count-1 {1 \a 2 \b})

(disj #{1 2 3})
(defn funcion-disj-1
  [a]
  (disj a))

(defn funcion-disj-2
  [a]
  (disj a))

(defn funcion-disj-3
  [a]
  (disj a))

(funcion-disj-1 [\a \b \c])
(funcion-disj-2 {\a \A \b \B})
(funcion-disj-3 #{\a \b \c})

(defn funcion-dissoc-1
  [a]
  (dissoc a))

(defn funcion-dissoc-2
  [a]
  (dissoc a))

(defn funcion-dissoc-3
  [a]
  (dissoc a))


(funcion-dissoc-1 {:a 1 :b 2 :c 3})
(funcion-dissoc-2 {\a \A \b \B \c \C})
(funcion-dissoc-3 #{\a \b \c})

(distinct [1 2 1 3 1 4 1 5])
(defn funcion-distinct-1
  [a]
  (distinct a))

(defn funcion-distinct-2
  [a]
  (distinct a))

(defn funcion-distinct-3
  [a]
  (distinct a))

(funcion-distinct-1 [\a \a \b \a \c \a])
(funcion-distinct-2 (vector \a \a \a \b \b \c))
(funcion-distinct-2 [])

(defn funcion-distinct?-1
  [a]
  (distinct? a))

(defn funcion-distinct?-2
  [a]
  (distinct? a))

(defn funcion-distinct?-3
  [a]
  (distinct? a))

(funcion-distinct?-1 [\a \b \c])
(funcion-distinct?-2 [1 2 3])
(funcion-distinct?-3 (vector \a \b \c \a \a \a \c))


(defn funcion-drop-last-1
  [a]
  (drop-last a))

(defn funcion-drop-last-2
  [a]
  (drop-last a))

(defn funcion-drop-last-3
  [a]
  (drop-last a))

(funcion-drop-last-1 [\a \b \c])
(funcion-drop-last-2 {\a \A \b \B})
(funcion-drop-last-3 #{\a \A \b \B})

(defn funcion-empty-1
  [a]
  (empty a))

(defn funcion-empty-2
  [a]
  (empty a))

(defn funcion-empty-3
  [a]
  (empty a))

(funcion-empty-1 [\a \b \c \d])
(funcion-empty-2 '(\A \B \C \D))
(funcion-empty-3 #{\a \A \b \B})


(defn funcion-empty?-1
  [a]
  (empty? a))

(defn funcion-empty?-2
  [a]
  (empty? a))

(defn funcion-empty?-3
  [a]
  (empty? a))

(funcion-empty?-1 ())
(funcion-empty?-2 [])
(funcion-empty?-3 #{})


(defn funcion-even?-1
  [a]
  (even? a))

(defn funcion-even?-2
  [a]
  (even? a))

(defn funcion-even?-3
  [a]
  (even? a))

(funcion-even?-1 1)
(funcion-even?-2 4)
(funcion-even?-2 21)

(defn funcion-false?-1
  [a]
  (false? a))

(defn funcion-false?-2
  [a]
  (false? a))

(defn funcion-false?-3
  [a]
  (false? a))

(funcion-false?-1 false)
(funcion-false?-2 true)
(funcion-false?-2 nil)

(defn funcion-find-1
  [a b]
  (find a b))

(defn funcion-find-2
  [a b]
  (find a b))

(defn funcion-find-3
  [a b]
  (find a b))

(funcion-find-1 {\a nil} \a)
(funcion-find-2 {\a \A \b \B} \b)
(funcion-find-2 {} \b)


(defn funcion-first-1
  [a]
  (first a))

(defn funcion-first-2
  [a]
  (first a))

(defn funcion-first-3
  [a]
  (first a))

(funcion-first-1 [\a \b \c])
(funcion-first-2 {\a \A \b \B})
(funcion-first-3 #{\a \A \b \B \c \C})

(defn funcion-de-flatten-1
  [a]
  (flatten a))

(defn funcion-de-flatten-2
  [a]
  (flatten a))

(defn funcion-de-flatten-3
  [a]
  (flatten a))


(funcion-de-flatten-1 '(\a \b (\c \d)))
(funcion-de-flatten-1 [\b [\a \c]])
(funcion-de-flatten-1 '(\d [\b [\a \c]]))

(defn funcion-frequencies-1
  [a]
  (frequencies a))

(defn funcion-frequencies-2
  [a]
  (frequencies a))

(defn funcion-frequencies-3
  [a]
  (frequencies a))

(funcion-frequencies-1 [\a \a \a \b])
(funcion-frequencies-1 {\a \b \c \d})
(funcion-frequencies-1 #{})

(defn funcion-get-1
  [a b]
  (get a b))

(defn funcion-get-2
  [a b]
  (get a b))

(defn funcion-get-3
  [a b]
  (get a b))

(funcion-get-1 [\a \b \c] 2)
(funcion-get-2 {\a \A} \a)
(funcion-get-3 #{\a \A \b \B} \a)


(defn funcion-get-in-1
  [a b]
  (get-in a b))

(defn funcion-get-in-2
  [a b]
  (get-in a b))

(defn funcion-get-in-3
  [a b]
  (get-in a b))

(funcion-get-in-1  {:a \a :b \b} [:a])
(funcion-get-in-2  [\a \b \c] [0])
(funcion-get-in-3  {:a \b :c \d} '())

(defn funcion-into-1
  [a b]
  (into a b))

(defn funcion-into-2
  [a b]
  (into a b))

(defn funcion-into-3
  [a b]
  (into a b))

(funcion-into-1 [\a \b \c] '(\c \d \A))
(funcion-into-2 [] {:a \a :b \b})
(funcion-into-2 () '(\a \b \c))

(defn funcion-key-1
  [a]
  (key a))

(defn funcion-key-2
  [a]
  (key a))

(defn funcion-key-3
  [a]
  (key a))

(map funcion-key-1 {:a \a :b \b})
(map funcion-key-2 {})
(map funcion-key-3 {\A \a \B \b \c \C})

(defn funcion-de-keys-1
  [a]
  (keys a))

(defn funcion-de-keys-2
  [a]
  (keys a))

(defn funcion-de-keys-3
  [a]
  (keys a))

(funcion-de-keys-1 {\a \A \b \B})
(funcion-de-keys-2 {})
(funcion-de-keys-2 nil)

(defn funcion-de-max-1
  [a b]
  (max a b))

(defn funcion-de-max-2
  [a b c]
  (max a b c))

(defn funcion-de-max-3
  [a b c d]
  (max a b c d))

(funcion-de-max-1 11 12)
(funcion-de-max-2 15 3 2)
(funcion-de-max-3 15 3 2 15)

(defn funciones-de-merge-1
  [a]
  (merge a))

(defn funciones-de-merge-2
  [a]
  (merge a))

(defn funciones-de-merge-3
  [a]
  (merge a))

(funciones-de-merge-1 {[\a 1] [\a 1]})
(funciones-de-merge-2 {\a \A \b \B})
(funciones-de-merge-2 {{\a \b} {}})

(defn funcion-de-min-1
  [a b]
  (min a b))

(defn funcion-de-min-2
  [a b c]
  (min a b c))

(defn funcion-de-min-3
  [a b c d]
  (min a b c d))
(funcion-de-min-1 2 1)
(funcion-de-min-2 12 5 8)
(funcion-de-min-3 12 5 4 8)

(defn funcion-de-neg?-1
  [a]
  (neg? a))

(defn funcion-de-neg?-2
  [a]
  (neg? a))

(defn funcion-de-neg?-3
  [a]
  (neg? a))

(funcion-de-neg?-1 -1)
(funcion-de-neg?-2 0)
(funcion-de-neg?-2 -1/2)

(defn funcion-de-nil?-1
  [a]
  (nil? a))

(defn funcion-de-nil?-2
  [a]
  (nil? a))

(defn funcion-de-nil?-3
  [a]
  (nil? a))
(funcion-de-nil?-1 nil)
(funcion-de-nil?-2 true)
(funcion-de-nil?-3 false)

(defn funcion-de-not-empty-1
  [a]
  (not-empty a))

(defn funcion-de-not-empty-2
  [a]
  (not-empty a))

(defn funcion-de-not-empty-3
  [a]
  (not-empty a))

(funcion-de-not-empty-1 [\a])
(funcion-de-not-empty-2 {[\a] [\b]})
(funcion-de-not-empty-3 {})


(defn funcion-de-nth-1
  [a b]
  (nth a b))

(defn funcion-de-nth-2
  [a b]
  (nth a b))

(defn funcion-de-nth-3
  [a b]
  (nth a b))

(funcion-de-nth-1 [\a \b] 0)
(funcion-de-nth-2 (vector \a \b \c) 2)
(funcion-de-nth-2 [\a \A \b \B] 1)

(defn funcion-de-odd?-1
  [a]
  (odd? a))

(defn funcion-de-odd?-2
  [a]
  (odd? a))

(defn funcion-de-odd?-3
  [a]
  (odd? a))

(funcion-de-odd?-1 2)
(funcion-de-odd?-2 27)
(funcion-de-odd?-3 0)

(defn funcion-de-partition-1
  [a b c]
  (partition a b c))

(defn funcion-de-partition-2
  [a b c]
  (partition a b c))

(defn funcion-de-partition-3
  [a b c]
  (partition a b c))

(funcion-de-partition-1 3 1 [\a \b \c \d \A \B])
(funcion-de-partition-2 2 1 [\a \b \c \d \A \B])
(funcion-de-partition-3 1 1 [\a \b \c \d \A \B \C \D])

(defn funcion-de-partition-all-1
  [a b]
  (partition-all a b))

(defn funcion-de-partition-all-2
  [a b c]
  (partition-all a b c))

(defn funcion-de-partition-all-3
  [a b]
  (partition-all a b))

(funcion-de-partition-all-1 2 [\a \b \c \d])
(funcion-de-partition-all-2 2 1 [\a \b \c \d])
(funcion-de-partition-all-3  4 [\a \b \c \d \A \B \C \D])

(defn funcion-de-peek-1
  [a]
  (peek a))

(defn funcion-de-peek-2
  [a]
  (peek a))

(defn funcion-de-peek-3
  [a]
  (peek a))

(funcion-de-peek-1 [\a \b])
(funcion-de-peek-2 '(\A \B \C))
(funcion-de-peek-3 '())

(defn funcion-de-pop-1
  [a]
  (pop a))

(defn funcion-de-pop-2
  [a]
  (pop a))

(defn funcion-de-pop-3
  [a]
  (pop a))

(funcion-de-pop-1 [\a \b \c])
(funcion-de-pop-2 '(\a \b \c))
(funcion-de-pop-3 (vector \a \b \c \d \a \b))

(defn funcion-de-pos?-1
  [a]
  (pos? a))

(defn funcion-de-pos?-2
  [a]
  (pos? a))

(defn funcion-de-pos?-3
  [a]
  (pos? a))

(funcion-de-pos?-1 1)
(funcion-de-pos?-2 0)
(funcion-de-pos?-3 -1)

(defn funcion-de-range-1
  [a]
  (range a))

(defn funcion-de-range-2
  [a b]
  (range a b))

(defn funcion-de-range-3
  [a b c]
  (range a b c))

(funcion-de-range-1 10)
(funcion-de-range-2 -5 5)
(funcion-de-range-3 -100 100 10)

(defn funcion-de-rem-1
  [a b]
  (rem a b))

(defn funcion-de-rem-2
  [a b]
  (rem a b))

(defn funcion-de-rem-3
  [a b]
  (rem a b))

(funcion-de-rem-1 10 9)
(funcion-de-rem-2  2 2)
(funcion-de-rem-3  -10 3)